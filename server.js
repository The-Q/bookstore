const express = require('express')
const app = express()
const MongoClient = require('mongodb').MongoClient
const db = require('./db')
const bodyParser = require('body-parser')
const port = 8000;

MongoClient.connect(db.url, (err, database) => {
    if (err) return console.log(err);
    app.use(bodyParser.json());
    require('./routes')(app, database);
    app.listen(port, () => {
        console.log('We are live on ' + port);
    });
})