import React, { Component } from 'react';
import { Table, Grid, Button, Icon } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom'

export default class AuthorsView extends Component {
    authors = [
        { id: 1, name: 'Jon Doe', bio: '12eqwdaw' },
        { id: 2, name: 'Jonh Doe', bio: '12eqwdaw' },
        { id: 3, name: 'Dave Doe', bio: '12eqwdaw' }
    ];
    render() {
        const _content = this.authors.map(function (author) {
            return (
                <Table.Row key={author.id}>
                    <Table.Cell>{author.name}</Table.Cell>
                    <Table.Cell>{author.bio}</Table.Cell>
                    <Table.Cell collapsing><Button circular
                        icon="angle right"
                        as={NavLink}
                        to={`${this.props.match.url}/view/${author.id}`}>
                    </Button>
                    </Table.Cell>
                </Table.Row>
            );
        }.bind(this));
        return (
            <Grid container >
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <Button as={NavLink} floated='right' to={`${this.props.match.url}/add`}> Добавить автора </Button>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={1}>
                    <Grid.Column>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>#</Table.HeaderCell>
                                    <Table.HeaderCell>Name</Table.HeaderCell>
                                    <Table.HeaderCell>Authors</Table.HeaderCell>
                                    <Table.HeaderCell></Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {_content}
                            </Table.Body>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='3'>

                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}