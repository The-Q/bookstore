import React, { Component } from 'react'
import { Menu } from 'semantic-ui-react'
import {
  BrowserRouter as Router,
  Route,
  NavLink
} from 'react-router-dom'
import AuthorsView from './AuthorsView'
import BooksView from './BooksView'
import AddAuthorForm from './AuthorAdd'
import AuthorCard from './AuthorView'

export default class HeaderNav extends Component {

  render() {
    return (
      <Router>
        <div>
          <Menu pointing secondary>
            <Menu.Item as={NavLink} to="/books" >Книги</Menu.Item>
            <Menu.Item as={NavLink} to="/authors" >Авторы</Menu.Item>
          </Menu>
          <Route path="/books" component={BooksView} />
          <Route exact path="/authors" component={AuthorsView} />
          <Route exact path="/authors/add" component={AddAuthorForm} />
          <Route path="/authors/view/:id" component={AuthorCard} />
          
        </div>
      </Router>
    )
  }
}