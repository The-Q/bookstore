import React, { Component } from 'react';
import { Form, Header } from 'semantic-ui-react';


export default class AddAuthorForm extends Component {
    render() {
        return (
            <div>
                <Header content="Create new author"/>
            <Form>
                <Form.Input label="Name" type="text" />
                <Form.Input label="Bio" type="textarea" />
            </Form>
            </div>
        )
    }
}
