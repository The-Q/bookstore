import React, { Component } from 'react'
import { Grid, Form, Button, Image } from 'semantic-ui-react';

export default class AuthorCard extends Component {
    render() {
        console.log(this.props.match.params );
        return (
            <Grid>
                <Grid.Row columns={2}>
                    <Grid.Column width={3}>
                        <Image shape='circular' src='' />
                    </Grid.Column>
                    <Grid.Column>
                        <Form>
                            <Form.Input label="Name" type="text" />
                            <Form.Input label="Bio" type="textarea" />
                        </Form>
                        
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}