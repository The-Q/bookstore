var mongo = require('mongodb');

module.exports = function (app, db) {
    app.get('/api/books/', function (req, res) {
        db.collection('books').aggregate([
            {
                $unwind: "$authors"
            },
            {
                $lookup:
                {
                    from: "authors",
                    localField: "authors",
                    foreignField: "_id",
                    as: "authors"
                }
            },
            {
                $project: { "authors.name": 1, "name": 1 }
            }
        ]
        ).toArray(function(err,items){
            res.json(items);
        })
    })
    app.get('/api/authors/', function (req, res) {
        db.collection('authors').find({}).toArray(function (err, items) {
            res.json(items);
        });
    })
    app.get('/api/books/:id', function (req, res) {
        const book_id = new mongo.ObjectID(req.params.id);
        db.collection('books').findOne({ _id: book_id })
            .then(function (doc) {
                res.json(doc);
            });
    })
    app.get('/api/authors/:id', function (req, res) {
        const author_id = new mongo.ObjectID(req.params.id);
        console.log(author_id);
        db.collection('authors').findOne({ _id: author_id })
            .then(function (doc) {
                res.json(doc);
            });


    })
    app.post('/api/authors/', function (req, res) {
        db.collection('authors')
            .insertOne(req.body)
            .then(function (r) {
                if (r.result.ok == 1) {
                    res.json(r.ops[0])
                } else {
                    res.status(500);
                    res.send("Try again later");
                }
            })
    })
    app.post('/api/books/', function (req, res) {
        var book = req.body;
        book.authors.map(a => new ObjectID(a));
        db.collection('books')
            .insertOne(book)
            .then(function (r) {
                if (r.result.ok == 1) {
                    res.json(r.ops[0])
                } else {
                    res.status(500);
                    res.send("Try again later");
                }
            });
    })
    app.post('/api/authors/:id', function (req, res) {
        const author_id = new mongo.ObjectID(req.params.id);
        db.collection('authors')
            .findOneAndReplace({ _id: author_id }, req.body)
            .then(function (r) {
                console.log(r);
                if (r.ok == 1) {
                    res.json(r.value)
                } else {
                    res.status(500);
                    res.send("Try again later");
                }
            })
    })
    app.post('/api/books/:id', function (req, res) {
        const book_id = new mongo.ObjectID(req.params.id);
        var book = req.body;
        book.authors.map(a => new ObjectID(a));
        db.collection('books')
            .findOneAndReplace({ _id: book_id }, book)
            .then(function (r) {
                if (r.ok == 1) {
                    res.json(r.value)
                } else {
                    res.status(500);
                    res.send("Try again later");
                }
            });
    })
    app.delete('/api/authors/:id', function (req, res) {
        const author_id = new mongo.ObjectID(req.params.id);
        db.collection('authors')
            .findOneAndDelete({ _id: author_id })
            .then(function (r) {
                console.log(r);
                if (r.ok == 1) {
                    res.json(r.value)
                } else {
                    res.status(500);
                    res.send("Try again later");
                }
            })
    })
    app.delete('/api/books/:id', function (req, res) {
        const book_id = new mongo.ObjectID(req.params.id);
        db.collection('books')
            .findOneAndDelete({ _id: book_id })
            .then(function (r) {
                if (r.ok == 1) {
                    res.json(r.value)
                } else {
                    res.status(500);
                    res.send("Try again later");
                }
            });
    })
};